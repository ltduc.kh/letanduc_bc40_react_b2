import React, { Component } from 'react'
import dataGlasses from '../Data/dataGlasses.json'
export default class BaiTapThuKinh extends Component {
    state ={
        glassesCurrent: {
            "id": 2,
            "price": 50,
            "name": "GUCCI G8759H",
            "url": "./glassesImage/v2.png",
            "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        }


    }
    renderGlassesList = () =>{
        return dataGlasses.map((glassesItem,index) =>{
            return <img onClick={() =>{this.changeGlasses(glassesItem)}} className="ml-2 p-2 border border-width-1" style={{width: '110px',cursor: 'pointer'}} key={index} src={glassesItem.url}/>
        })
    }
    changeGlasses = (newGlasses)=>{
        this.setState({
            glassesCurrent: newGlasses
        })


    }
    render() {
        const styleGlasses = {
            width: '150px',
            top: '70px',
            right: '70px',
            opacity: '.7'
        }
        const infoGlasses ={
            width: '250px',
            top: '200px',
            left: '270px',
            paddingLeft: '15px',
            backgroundColor: 'rgba(255,127,0,.5)',
            textAlign: 'left',
            height: '105px'


        }
        return (
            <div style={{ backgroundImage: 'url(./glassesImage/background.jpg)', backgroundSize: '2000px', minHeight: '2000px' }}>
                <div style={{ backgroundColor: 'rgba(0,0,0,.8)', minHeight: '2000px' }}>
                    <h2 style={{ backgroundImage: 'rgba(0,0,0,.3)' }} className='text-center text-light p-5'>Try Glasses App Online</h2>
                    <div className="container">
                        <div className='row mt-5 text-center'>
                            <div className='col-6'>
                                <div className="position-relative">
                                    
                                
                                    <img className="position-absolute" style={{ width: '250px' }} src="./glassesImage/model.jpg" alt="" />
                                    <img style={styleGlasses} className="position-absolute"  src={this.state.glassesCurrent.url} />
                                    <div style={infoGlasses} className="position-relative ">
                                        <span style={{color: '#AB82FF', fontSize: '17px'}} className="font-weight-bold">{this.state.glassesCurrent.name}
                                        </span><br/>
                                        <p style={{fontSize: '13px', fontWeight: '400'}}>{this.state.glassesCurrent.desc} </p>
                                    </div>
                                    </div>
                                </div>
                                <div className='col-6'>
                                    <img  style={{ width: '250px' }} src="./glassesImage/model.jpg" alt="" />
                                    
                                </div>
                                </div>
                            
                        </div>
                        <div className="bg-light container text-center mt-5 d-flex justìy-content-center">
                            {this.renderGlassesList()}
                            
                        </div>
                    </div>
                </div>
        )
    }
}
